<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\StudentRepository")
 */
class Student
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Classe", inversedBy="students")
     * @ORM\JoinColumn(nullable=false)
     */
    private $classe;

    /**
     * @ORM\Column(type="string", length=20)
     */
    private $firstname;

    /**
     * @ORM\Column(type="string", length=20)
     */
    private $lastname;

    /**
     * @ORM\Column(type="decimal", precision=3, scale=1)
     */
    private $moyenne = 0;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\RenduEvaluation", mappedBy="student")
     */
    private $rendus;

    public function __construct()
    {
        $this->rendus = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->getName();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getClasse(): ?Classe
    {
        return $this->classe;
    }

    public function setClasse(?Classe $classe): self
    {
        $this->classe = $classe;

        return $this;
    }

    public function getFirstname(): ?string
    {
        return $this->firstname;
    }

    public function setFirstname(string $firstname): self
    {
        $this->firstname = $firstname;

        return $this;
    }

    public function getLastname(): ?string
    {
        return $this->lastname;
    }

    public function setLastname(string $lastname): self
    {
        $this->lastname = $lastname;

        return $this;
    }

    public function getName(): string
    {
        return strtoupper($this->lastname).' '.$this->firstname;
    }

    public function getMoyenne()
    {
        return $this->moyenne;
    }

    public function setMoyenne($moyenne): self
    {
        $this->moyenne = $moyenne;

        return $this;
    }

    /**
     * @return Collection|RenduEvaluation[]
     */
    public function getRendus(): Collection
    {
        return $this->rendus;
    }

    public function addRendus(RenduEvaluation $rendus): self
    {
        if (!$this->rendus->contains($rendus)) {
            $this->rendus[] = $rendus;
            $rendus->setStudent($this);
        }

        return $this;
    }

    public function removeRendus(RenduEvaluation $rendus): self
    {
        if ($this->rendus->contains($rendus)) {
            $this->rendus->removeElement($rendus);
            // set the owning side to null (unless already changed)
            if ($rendus->getStudent() === $this) {
                $rendus->setStudent(null);
            }
        }

        return $this;
    }
}
