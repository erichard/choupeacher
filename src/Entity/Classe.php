<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ClasseRepository")
 */
class Classe
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=5)
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Student", mappedBy="classe")
     * @ORM\OrderBy({"lastname"="ASC"})
     */
    private $students;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Evaluation", mappedBy="classe")
     * @ORM\OrderBy({"occuredAt"="DESC"})
     */
    private $evaluations;

    /**
     * @ORM\Column(type="decimal", precision=3, scale=1)
     */
    private $moyenne;

    public function __construct()
    {
        $this->students = new ArrayCollection();
        $this->evaluations = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->name;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|Student[]
     */
    public function getStudents(): Collection
    {
        return $this->students;
    }

    public function addStudent(Student $student): self
    {
        if (!$this->students->contains($student)) {
            $this->students[] = $student;
            $student->setClasse($this);
        }

        return $this;
    }

    public function removeStudent(Student $student): self
    {
        if ($this->students->contains($student)) {
            $this->students->removeElement($student);
            // set the owning side to null (unless already changed)
            if ($student->getClasse() === $this) {
                $student->setClasse(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Evaluation[]
     */
    public function getEvaluations(): Collection
    {
        return $this->evaluations;
    }

    public function getDerniereEvaluation(): ?Evaluation
    {
        return count($this->evaluations) > 0 ?
            $this->evaluations->last() :
            null
        ;
    }

    public function addEvaluation(Evaluation $evaluation): self
    {
        if (!$this->evaluations->contains($evaluation)) {
            $this->evaluations[] = $evaluation;
            $evaluation->setClasse($this);
        }

        return $this;
    }

    public function removeEvaluation(Evaluation $evaluation): self
    {
        if ($this->evaluations->contains($evaluation)) {
            $this->evaluations->removeElement($evaluation);
            // set the owning side to null (unless already changed)
            if ($evaluation->getClasse() === $this) {
                $evaluation->setClasse(null);
            }
        }

        return $this;
    }

    public function getMoyenne()
    {
        return $this->moyenne;
    }

    public function setMoyenne($moyenne): self
    {
        $this->moyenne = $moyenne;

        return $this;
    }
}
