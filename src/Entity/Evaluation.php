<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\EvaluationRepository")
 */
class Evaluation
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
     * @Assert\NotBlank(groups={"admin.creation"})
     */
    private $name;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Classe", inversedBy="evaluations")
     * @ORM\JoinColumn(nullable=false)
     */
    private $classe;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $occuredAt;

    /**
     * @ORM\Column(type="decimal", precision=3, scale=1, nullable=true)
     */
    private $moyenne;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\RenduEvaluation", mappedBy="evaluation", cascade={"persist"})
     */
    private $rendus;

    /**
     * @ORM\Column(type="decimal", precision=3, scale=1, nullable=true)
     */
    private $moyenneAvant;

    /**
     * @ORM\Column(type="decimal", precision=3, scale=1, nullable=true)
     */
    private $moyenneApres;

    public function __construct()
    {
        $this->rendus = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->name;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getClasse(): ?Classe
    {
        return $this->classe;
    }

    public function setClasse(?Classe $classe): self
    {
        $this->classe = $classe;

        return $this;
    }

    public function getOccuredAt(): ?\DateTimeInterface
    {
        return $this->occuredAt;
    }

    public function setOccuredAt(?\DateTimeInterface $occuredAt): self
    {
        $this->occuredAt = $occuredAt;

        return $this;
    }

    public function getProgression(): int
    {
        return count($this->rendus) / count($this->classe->getStudents()) * 100;
    }

    public function getProgressionAsString(): string
    {
        return count($this->rendus).'/'.count($this->classe->getStudents());
    }

    public function getMoyenne()
    {
        return $this->moyenne;
    }

    public function setMoyenne($moyenne): self
    {
        $this->moyenne = $moyenne;

        return $this;
    }

    /**
     * @return Collection|RenduEvaluation[]
     */
    public function getRendus(): Collection
    {
        return $this->rendus;
    }

    public function addRendu(RenduEvaluation $renduEvaluation): self
    {
        if (!$this->rendus->contains($renduEvaluation)) {
            $this->rendus[] = $renduEvaluation;
            $renduEvaluation->setEvaluation($this);
        }

        return $this;
    }

    public function removeRendu(RenduEvaluation $renduEvaluation): self
    {
        if ($this->rendus->contains($renduEvaluation)) {
            $this->rendus->removeElement($renduEvaluation);
            // set the owning side to null (unless already changed)
            if ($renduEvaluation->getEvaluation() === $this) {
                $renduEvaluation->setEvaluation(null);
            }
        }

        return $this;
    }

    public function getMoyenneAvant()
    {
        return $this->moyenneAvant;
    }

    public function setMoyenneAvant($moyenneAvant): self
    {
        $this->moyenneAvant = $moyenneAvant;

        return $this;
    }

    public function getMoyenneApres()
    {
        return $this->moyenneApres;
    }

    public function setMoyenneApres($moyenneApres): self
    {
        $this->moyenneApres = $moyenneApres;

        return $this;
    }

    public function getCommentairesDifferents()
    {
        $commentaires = [];

        foreach ($this->rendus as $rendu) {
            if (!in_array($rendu->getCommentaires(), $commentaires)) {
                $commentaires[] = $rendu->getCommentaires();
            }
        }

        return $commentaires;
    }

    public function getConseilsDifferents()
    {
        $conseils = [];

        foreach ($this->rendus as $rendu) {
            if (!in_array($rendu->getConseils(), $conseils)) {
                $conseils[] = $rendu->getConseils();
            }
        }

        return $conseils;
    }

    public function getAttitudeDifferents()
    {
        $attitude = [];

        foreach ($this->rendus as $rendu) {
            if (!in_array($rendu->getAttitude(), $attitude)) {
                $attitude[] = $rendu->getAttitude();
            }
        }

        return $attitude;
    }
}
