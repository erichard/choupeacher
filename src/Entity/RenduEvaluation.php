<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\RenduEvaluationRepository")
 */
class RenduEvaluation
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="decimal", precision=3, scale=1)
     */
    private $note;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $commentaires;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $conseils;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $attitude;

    /**
     * @ORM\Column(type="boolean")
     */
    private $alerteCours = false;

    /**
     * @ORM\Column(type="boolean")
     */
    private $alerteComportement = false;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Evaluation", inversedBy="renduEvaluations")
     * @ORM\JoinColumn(nullable=false)
     */
    private $evaluation;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Student", inversedBy="rendus")
     * @ORM\JoinColumn(nullable=false)
     */
    private $student;

    /**
     * @ORM\Column(type="decimal", precision=3, scale=1)
     */
    private $moyenneAvant;

    /**
     * @ORM\Column(type="decimal", precision=3, scale=1)
     */
    private $moyenneApres;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->evaluation->getName().' - '.$this->student->getName();
    }

    public function getNote()
    {
        return $this->note;
    }

    public function setNote($note): self
    {
        $this->note = $note;

        return $this;
    }

    public function getCommentaires(): ?string
    {
        return $this->commentaires;
    }

    public function setCommentaires(?string $commentaires): self
    {
        $this->commentaires = $commentaires;

        return $this;
    }

    public function getConseils(): ?string
    {
        return $this->conseils;
    }

    public function setConseils(?string $conseils): self
    {
        $this->conseils = $conseils;

        return $this;
    }

    public function isAlerteCours(): ?bool
    {
        return $this->alerteCours;
    }

    public function setAlerteCours(bool $alerteCours): self
    {
        $this->alerteCours = $alerteCours;

        return $this;
    }

    public function isAlerteComportement(): ?bool
    {
        return $this->alerteComportement;
    }

    public function setAlerteComportement(bool $alerteComportement): self
    {
        $this->alerteComportement = $alerteComportement;

        return $this;
    }

    public function getEvaluation(): ?Evaluation
    {
        return $this->evaluation;
    }

    public function setEvaluation(?Evaluation $evaluation): self
    {
        $this->evaluation = $evaluation;

        return $this;
    }

    public function getStudent(): ?Student
    {
        return $this->student;
    }

    public function setStudent(?Student $student): self
    {
        $this->student = $student;

        return $this;
    }

    public function getMoyenneAvant()
    {
        return $this->moyenneAvant;
    }

    public function setMoyenneAvant($moyenneAvant): self
    {
        $this->moyenneAvant = $moyenneAvant;

        return $this;
    }

    public function getMoyenneApres()
    {
        return $this->moyenneApres;
    }

    public function setMoyenneApres($moyenneApres): self
    {
        $this->moyenneApres = $moyenneApres;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getAttitude()
    {
        return $this->attitude;
    }

    /**
     * @param mixed $attitude
     *
     * @return self
     */
    public function setAttitude($attitude)
    {
        $this->attitude = $attitude;

        return $this;
    }
}
