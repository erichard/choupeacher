<?php

namespace App\Twig;

use App\Service\GradeAsColor;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

class AppExtension extends AbstractExtension
{
    private $gradeAsColor;

    public function __construct(GradeAsColor $gradeAsColor)
    {
        $this->gradeAsColor = $gradeAsColor;
    }

    public function getFilters(): array
    {
        return [
            new TwigFilter('asColor', [$this, 'asColor']),
            new TwigFilter('asColorHex', [$this, 'asColorHex']),
        ];
    }

    public function asColor($value)
    {
        return $this->gradeAsColor->convertAsColor($value);
    }

    public function asColorHex($value)
    {
        return $this->gradeAsColor->convertAsHexColor($value);
    }
}
