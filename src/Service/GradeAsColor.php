<?php

namespace App\Service;

class GradeAsColor
{
    public function convertAsColor($grade): string
    {
        switch (true) {
            case $grade >= 16:
                return 'success';
            case $grade >= 9:
                return 'warning';
            default:
                return 'danger';
        }
    }

    public function convertAsHexColor($grade): string
    {
        switch (true) {
            case $grade >= 16:
                return '#66cd00';
            case $grade >= 9:
                return '#ffa500';
            default:
                return '#b22222';
        }
    }
}
