<?php

namespace App\Service;

use App\Entity\Classe;
use App\Entity\Evaluation;
use App\Entity\RenduEvaluation;
use App\Entity\Student;

class MoyenneCalculator
{
    public function calculeMoyenneRendu(RenduEvaluation $rendu)
    {
        $student = $rendu->getStudent();

        $rendu->setMoyenneAvant($student->getMoyenne());
        $this->calculeMoyenneStudent($student);
        $rendu->setMoyenneApres($student->getMoyenne());
    }

    public function calculeMoyenneStudent(Student $student)
    {
        $notes = [];

        foreach ($student->getRendus() as $rendu) {
            $notes[] = $rendu->getNote();
        }

        if (count($notes) > 0) {
            $student->setMoyenne(array_sum($notes) / count($notes));
        }
    }

    public function calculeMoyenneEvaluation(Evaluation $evaluation)
    {
        $notes = [];

        foreach ($evaluation->getRendus() as $rendu) {
            $notes[] = $rendu->getNote();
        }

        if (count($notes) > 0) {
            $evaluation->setMoyenne(array_sum($notes) / count($notes));
        }

        $classe = $evaluation->getClasse();
        $evaluation->setMoyenneAvant($classe->getMoyenne());
        $this->calculMoyenneClasse($classe);
        $evaluation->setMoyenneApres($classe->getMoyenne());
    }

    public function calculMoyenneClasse(Classe $classe)
    {
        $notes = [];

        foreach ($classe->getEvaluations() as $evaluation) {
            foreach ($evaluation->getRendus() as $rendu) {
                $notes[] = $rendu->getNote();
            }
        }

        if (count($notes) > 0) {
            $classe->setMoyenne(array_sum($notes) / count($notes));
        }
    }
}
