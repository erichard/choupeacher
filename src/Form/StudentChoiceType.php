<?php

namespace App\Form;

use App\Entity\Student;
use Faros\Bundle\AdminBundle\Form\Type\AutocompleteType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class StudentChoiceType extends AbstractType
{
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'class' => Student::class,
            'route' => 'admin_student_list',
            'property' => 'name',
            'placeholder' => 'Choisir un élève',
            'limit' => 35,
            'min_length' => 0,
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getParent()
    {
        return AutocompleteType::class;
    }
}
