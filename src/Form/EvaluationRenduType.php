<?php

namespace App\Form;

use App\Entity\RenduEvaluation;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EvaluationRenduType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('note')
            ->add('commentaires')
            ->add('conseils')
            ->add('attitude')
            ->add('alerteCours')
            ->add('alerteComportement')
            ->add('student', StudentChoiceType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => RenduEvaluation::class,
        ]);
    }
}
