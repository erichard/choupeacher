<?php

namespace App\Controller\Admin;

use App\Entity\Student;
use App\Form\StudentType;
use App\Repository\StudentRepository;
use Faros\Bundle\AdminBundle\Controller\CRUD\ORMController;
use Symfony\Component\HttpFoundation\Request;

class StudentController extends ORMController
{
    private $repository;

    public function __construct(StudentRepository $repository)
    {
        $this->repository = $repository;
    }

    protected function getName()
    {
        return 'student';
    }

    protected function getRolePrefix()
    {
        return 'ROLE_ADMIN_STUDENT_';
    }

    protected function getRoutePrefix()
    {
        return 'admin_student_';
    }

    protected function getEntityName()
    {
        return Student::class;
    }

    public function getColumns()
    {
        return [
            [
                'data' => 'id',
                'field_type' => 'integer',
                'visible' => false,
            ],
            [
                'data' => 'lastname',
            ],
            [
                'data' => 'firstname',
            ],
            [
                'data' => 'name',
                'visible' => false,
            ],
            [
                'data' => 'classe.name',
                'field' => 'c.name',
                'orderData' => [1, 2],
                'order' => 'ASC',
            ],
        ];
    }

    protected function getQueryBuilder(Request $request, array $filters = [])
    {
        $term = $request->request->get('autocomplete-term');

        $filters['autocomplete-term'] = '' === $term ? null : $term;
        $filters['classe'] = $request->request->get('classe', null);

        return $this
            ->repository
            ->createQueryBuilderForAdmin($filters)
        ;
    }

    protected function getFormType()
    {
        return StudentType::class;
    }

    protected function findEntity(Request $request)
    {
        $id = $request->attributes->get('id', $request->attributes->get('sortItem', 0));

        return $this
            ->repository
            ->findWithRendusSortedByDate($id)
        ;
    }
}
