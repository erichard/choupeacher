<?php

namespace App\Controller\Admin;

use App\Entity\Evaluation;
use App\Entity\RenduEvaluation;
use App\Form\EvaluationRenduType;
use App\Form\EvaluationType;
use App\Repository\ClasseRepository;
use App\Repository\EvaluationRepository;
use App\Repository\RenduEvaluationRepository;
use App\Service\MoyenneCalculator;
use Faros\Bundle\AdminBundle\Controller\CRUD\ORMController;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

class EvaluationController extends ORMController
{
    private $classeRepository;
    private $evaluationRepository;
    private $moyenneCalculator;
    private $renduRepository;
    private $pdfPath;
    private $tmpDir;

    public function __construct(ClasseRepository $classeRepository, EvaluationRepository $evaluationRepository, MoyenneCalculator $moyenneCalculator, RenduEvaluationRepository $renduRepository, string $pdfPath, string $tmpDir)
    {
        $this->classeRepository = $classeRepository;
        $this->evaluationRepository = $evaluationRepository;
        $this->moyenneCalculator = $moyenneCalculator;
        $this->renduRepository = $renduRepository;
        $this->pdfPath = $pdfPath;
        $this->tmpDir = $tmpDir;
    }

    protected function getName()
    {
        return 'evaluation';
    }

    protected function getRolePrefix()
    {
        return 'ROLE_ADMIN_EVALUATION_';
    }

    protected function getRoutePrefix()
    {
        return 'admin_evaluation_';
    }

    protected function getEntityName()
    {
        return Evaluation::class;
    }

    public function getColumns()
    {
        return [
            [
                'data' => 'id',
                'field_type' => 'integer',
            ],

            [
                'data' => 'name',
            ],
            [
                'data' => 'occuredAt',
            ],
        ];
    }

    protected function createEntity(Request $request, $classname)
    {
        $entity = parent::createEntity($request, $classname);

        $classe = $this->classeRepository->findOneByName($request->query->get('classe'));

        $entity->setClasse($classe);

        return $entity;
    }

    protected function getFormType()
    {
        return EvaluationType::class;
    }

    public function summary($id)
    {
        $evaluation = $this->evaluationRepository->findWithRenduSortedByStudent($id);

        if (null === $evaluation) {
            return $this->createNotFoundException();
        }

        return $this->render('Admin/Evaluation/summary.html.twig', [
            'evaluation' => $evaluation,
        ]);
    }

    public function rendu(Request $request, Evaluation $evaluation)
    {
        if ($request->query->has('rendu')) {
            $rendu = $this->renduRepository->find($request->query->getInt('rendu'));
        } else {
            $rendu = new RenduEvaluation();
            $rendu->setEvaluation($evaluation);
        }

        $form = $this->createForm(EvaluationRenduType::class, $rendu);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $rendu = $form->getData();
            $evaluation->addRendu($rendu);

            $this->moyenneCalculator->calculeMoyenneRendu($rendu);
            $this->moyenneCalculator->calculeMoyenneEvaluation($evaluation);
            $this->evaluationRepository->save($evaluation);

            return $this->redirectToRoute('admin_evaluation_rendu', ['id' => $evaluation->getId()]);
        }

        return $this->render('Admin/Evaluation/rendu.html.twig', [
            'evaluation' => $evaluation,
            'form' => $form->createView(),
        ]);
    }

    public function printRendu(Request $request, Evaluation $evaluation)
    {
        $rendus = $this->renduRepository->findByEvaluation($evaluation);

        $filename = $this->tmpDir.'/'
            .sprintf('rendu-%s-%s.zip', $evaluation->getClasse()->getName(), $evaluation->getOccuredAt()->format('Y-m-d'))
        ;

        if (is_file($this->pdfPath.'/rendu.pdf')) {
            unlink($this->pdfPath.'/rendu.pdf');
        }

        copy($this->pdfPath.'/rendu_blank.pdf', $this->pdfPath.'/rendu.pdf');

        $pdfComplet = new \mikehaertl\pdftk\Pdf($this->pdfPath.'/rendu.pdf');

        $zip = new \ZipArchive();
        $error = $zip->open($filename, \ZipArchive::CREATE | \ZipArchive::OVERWRITE);

        if (true !== $error) {
            $ZIP_ERROR = [
              \ZipArchive::ER_EXISTS => 'File already exists.',
              \ZipArchive::ER_INCONS => 'Zip archive inconsistent.',
              \ZipArchive::ER_INVAL => 'Invalid argument.',
              \ZipArchive::ER_MEMORY => 'Malloc failure.',
              \ZipArchive::ER_NOENT => 'No such file.',
              \ZipArchive::ER_NOZIP => 'Not a zip archive.',
              \ZipArchive::ER_OPEN => "Can't open file.",
              \ZipArchive::ER_READ => 'Read error.',
              \ZipArchive::ER_SEEK => 'Seek error.',
            ];

            throw new \RuntimeException('Impossible de créer un zip. Code '.$ZIP_ERROR[$error]);
        }

        foreach ($rendus as $rendu) {
            if ($rendu->isAlerteCours() && $rendu->isAlerteComportement()) {
                $pdf = 'Fiche_badge_cours_attitude.pdf';
            } elseif ($rendu->isAlerteCours()) {
                $pdf = 'Fiche_badge_cours.pdf';
            } elseif ($rendu->isAlerteComportement()) {
                $pdf = 'Fiche_badge_attitude.pdf';
            } else {
                $pdf = 'Fiche_sans_badge.pdf';
            }

            $pdf = new \mikehaertl\pdftk\Pdf($this->pdfPath.'/'.$pdf);

            $tempName = tempnam($this->tmpDir, 'pdf_');

            $pdf
                ->fillForm([
                    'Prénom' => $rendu->getStudent()->getFirstname(),
                    'Date' => $evaluation->getOccuredAt()->format('d/m/Y'),
                    'Note' => $rendu->getNote(),
                    'Sujet' => $evaluation->getName(),
                    'Commentaires' => $rendu->getCommentaires(),
                    'Conseils' => $rendu->getConseils(),
                    'Attitude' => $rendu->getAttitude(),
                ])
                ->needAppearances()
            ;

            if (!$pdf->saveAs($tempName)) {
                throw new \RuntimeException($pdf->getError());
            }

            $pdfComplet->addFile($tempName);
            $zip->addFile($tempName, $rendu->getStudent()->getFirstname().'_'.$rendu->getStudent()->getLastname()[0].'.pdf');
        }

        $pdfComplet->saveAs($this->pdfPath.'/rendu.pdf');
        $zip->addFile($this->pdfPath.'/rendu.pdf', '00-rendu.pdf');
        $zip->close();

        $response = new BinaryFileResponse($filename);
        $response->deleteFileAfterSend(true);
        $response->setContentDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT,
            basename($filename)
        );

        return $response;
    }
}
