<?php

namespace App\Controller\Admin;

use App\Entity\RenduEvaluation;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class RenduController extends AbstractController
{
    public function show(RenduEvaluation $rendu)
    {
        return $this->render('Admin/Rendu/show.html.twig', [
            'rendu' => $rendu,
        ]);
    }
}
