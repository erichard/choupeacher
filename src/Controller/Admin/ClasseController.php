<?php

namespace App\Controller\Admin;

use App\Entity\Classe;
use App\Repository\ClasseRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ClasseController extends AbstractController
{
    public function index(ClasseRepository $classeRepository)
    {
        $classes = $classeRepository->findAll();

        return $this->render('Admin/Classe/index.html.twig', [
            'classes' => $classes,
        ]);
    }

    public function show(Classe $classe)
    {
        return $this->render('Admin/Classe/show.html.twig', [
            'entity' => $classe,
        ]);
    }
}
