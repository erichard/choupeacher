<?php

namespace App\Repository;

use App\Entity\Evaluation;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Evaluation|null find($id, $lockMode = null, $lockVersion = null)
 * @method Evaluation|null findOneBy(array $criteria, array $orderBy = null)
 * @method Evaluation[]    findAll()
 * @method Evaluation[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EvaluationRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Evaluation::class);
    }

    public function findWithRenduSortedByStudent($evaluation)
    {
        return $this->createQueryBuilder('e')
            ->addSelect('v')
            ->addSelect('c')
            ->leftJoin('e.classe', 'c')
            ->leftJoin('e.rendus', 'v')
            ->leftJoin('v.student', 's')
            ->orderBy('s.lastname', 'ASC')
            ->where('e.id = :id')
            ->setParameter(':id', $evaluation)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }

    public function save(Evaluation $evaluation)
    {
        $this->_em->persist($evaluation);
        $this->_em->flush();
    }
}
