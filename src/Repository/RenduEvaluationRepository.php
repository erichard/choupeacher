<?php

namespace App\Repository;

use App\Entity\RenduEvaluation;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method RenduEvaluation|null find($id, $lockMode = null, $lockVersion = null)
 * @method RenduEvaluation|null findOneBy(array $criteria, array $orderBy = null)
 * @method RenduEvaluation[]    findAll()
 * @method RenduEvaluation[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RenduEvaluationRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, RenduEvaluation::class);
    }

    // /**
    //  * @return RenduEvaluation[] Returns an array of RenduEvaluation objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?RenduEvaluation
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
