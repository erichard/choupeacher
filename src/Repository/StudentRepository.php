<?php

namespace App\Repository;

use App\Entity\Student;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Symfony\Bridge\Doctrine\RegistryInterface;

class StudentRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Student::class);
    }

    public function createQueryBuilderForAdmin(array $filters): QueryBuilder
    {
        $qb = $this->createQueryBuilder('s')
            ->addSelect('c')
            ->join('s.classe', 'c')
            ->addOrderBy('s.lastname', 'ASC')
            ->addOrderBy('s.firstname', 'ASC')
        ;

        if (isset($filters['classe'])) {
            $qb
                ->andWhere('s.classe = :classe')
                ->setParameter('classe', $filters['classe'])
            ;
        }

        if (isset($filters['autocomplete-term'])) {
            $qb
                ->andWhere('(LOWER(s.firstname) LIKE :term OR LOWER(s.lastname) LIKE :term)')
                ->setParameter('term', strtolower($filters['autocomplete-term']).'%')
            ;
        }

        if (isset($filters['autocomplete-id'])) {
            $qb
                ->andWhere('s.id IN(:id) ')
                ->setParameter('id', $filters['autocomplete-id'])
            ;
        }

        return $qb;
    }

    public function findWithRendusSortedByDate($id)
    {
        return $this->createQueryBuilder('s')
            ->addSelect('c')
            ->addSelect('r')
            ->join('s.classe', 'c')
            ->join('s.rendus', 'r')
            ->join('r.evaluation', 'e')
            ->orderBy('e.occuredAt', 'DESC')
            ->where('s.id = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
}
