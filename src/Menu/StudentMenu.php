<?php

namespace App\Menu;

use Faros\Bundle\AdminBundle\Menu\MenuBuilderTrait;
use Faros\Bundle\AdminBundle\Menu\MenuSectionInterface;
use Knp\Menu\ItemInterface;

class StudentMenu implements MenuSectionInterface
{
    use MenuBuilderTrait;

    public function createMenu(): ItemInterface
    {
        $menu = $this->factory->createItem('menu.students', [
            'extras' => [
                'icon_class' => 'fa fa-graduation-cap',
            ],
            'childrenAttributes' => [
                'id' => 'system',
                'class' => 'acc-menu',
            ],
            'route' => 'admin_student_list',
        ]);

        $menu->setDisplay($this->isGranted('ROLE_ADMIN_STUDENT_LIST'));

        return $menu;
    }
}
