<?php

namespace App\Menu;

use Faros\Bundle\AdminBundle\Menu\MenuBuilderTrait;
use Faros\Bundle\AdminBundle\Menu\MenuSectionInterface;
use Knp\Menu\ItemInterface;

class ClasseMenu implements MenuSectionInterface
{
    use MenuBuilderTrait;

    public function createMenu(): ItemInterface
    {
        $menu = $this->factory->createItem('menu.classes', [
            'extras' => [
                'icon_class' => 'fa fa-bell',
            ],
            'childrenAttributes' => [
                'id' => 'system',
                'class' => 'acc-menu',
            ],
            'route' => 'admin_classe_list',
        ]);

        $menu->setDisplay($this->isGranted('ROLE_ADMIN_CLASSE_LIST'));

        return $menu;
    }
}
