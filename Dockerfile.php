FROM lephare/php:7.3

RUN apt-get update \
    && mkdir -p /usr/share/man/man1 \
    && mkdir -p /usr/share/man/man7 \
    && apt-get install -y --no-install-recommends \
        postgresql-client-9.6 pdftk \
    && rm -rf /var/lib/apt/lists/* \
    && apt-get clean
