SF = docker-compose run --rm php bin/console

## Symfony stuff

refresh:
	@$(SF) doctrine:database:drop --force --if-exists
	@$(SF) doctrine:database:create
	@$(SF) doctrine:schema:create
	@$(SF) hautelook:fixtures:load --no-interaction

cache-clear:
	@$(SF) cache:clear --no-warmup

cache-warmup: cache-clear
	@$(SF) cache:warmup

.PHONY: cache-clear cache-warmup
